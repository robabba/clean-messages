# Clean Messages #

## How to use ##
* Import the .DLL into your project by right clicking References > Add new reference...
* To use the library type at the top:

```
#!c#

using Clean_Messages;
```
* Then you can start using it:

```
#!c#

CleanMessage myAmazingMessage = new CleanMessage();

// You can null any part of the message box if you do not need it. Eg, you may not need Button 3
myAmazingMessage.CleanMessageBoxDefault("My Amazing Title", "This is my Amazing Message!", "Button 1", "Button 2", "Button 3");
myAmazingMessage.ShowDialog();
```
**You can check out more examples in the CleanMessagesExample project**

### Licensed under The MIT License ###