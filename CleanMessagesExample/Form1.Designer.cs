﻿namespace CleanMessagesExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDefault = new System.Windows.Forms.Button();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnWarning = new System.Windows.Forms.Button();
            this.btnError = new System.Windows.Forms.Button();
            this.btnQuestion = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.chkIcon = new System.Windows.Forms.CheckBox();
            this.txtIcon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnIconBrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStyle = new System.Windows.Forms.ComboBox();
            this.grpCustom = new System.Windows.Forms.GroupBox();
            this.txtR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtG = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCustomBrowse = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCustom = new System.Windows.Forms.TextBox();
            this.chkCustom = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDefault = new System.Windows.Forms.TextBox();
            this.chkAlt = new System.Windows.Forms.CheckBox();
            this.txtAlt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOptional = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkOptional = new System.Windows.Forms.CheckBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbTreated = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpCustom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDefault
            // 
            this.btnDefault.Location = new System.Drawing.Point(15, 29);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(132, 23);
            this.btnDefault.TabIndex = 0;
            this.btnDefault.Text = "Default Style";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.Location = new System.Drawing.Point(15, 58);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(132, 23);
            this.btnInfo.TabIndex = 1;
            this.btnInfo.Text = "Info Style";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnWarning
            // 
            this.btnWarning.Location = new System.Drawing.Point(15, 87);
            this.btnWarning.Name = "btnWarning";
            this.btnWarning.Size = new System.Drawing.Size(132, 23);
            this.btnWarning.TabIndex = 2;
            this.btnWarning.Text = "Warning Style";
            this.btnWarning.UseVisualStyleBackColor = true;
            this.btnWarning.Click += new System.EventHandler(this.btnWarning_Click);
            // 
            // btnError
            // 
            this.btnError.Location = new System.Drawing.Point(15, 116);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(132, 23);
            this.btnError.TabIndex = 4;
            this.btnError.Text = "Error Style";
            this.btnError.UseVisualStyleBackColor = true;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // btnQuestion
            // 
            this.btnQuestion.Location = new System.Drawing.Point(15, 145);
            this.btnQuestion.Name = "btnQuestion";
            this.btnQuestion.Size = new System.Drawing.Size(132, 23);
            this.btnQuestion.TabIndex = 3;
            this.btnQuestion.Text = "Question Style";
            this.btnQuestion.UseVisualStyleBackColor = true;
            this.btnQuestion.Click += new System.EventHandler(this.btnQuestion_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnQuestion);
            this.groupBox1.Controls.Add(this.btnError);
            this.groupBox1.Controls.Add(this.btnDefault);
            this.groupBox1.Controls.Add(this.btnInfo);
            this.groupBox1.Controls.Add(this.btnWarning);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(163, 184);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Premade";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.picImage);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.picIcon);
            this.groupBox2.Controls.Add(this.btnReset);
            this.groupBox2.Controls.Add(this.btnCreate);
            this.groupBox2.Controls.Add(this.chkOptional);
            this.groupBox2.Controls.Add(this.txtOptional);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtAlt);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.chkAlt);
            this.groupBox2.Controls.Add(this.txtDefault);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.grpCustom);
            this.groupBox2.Controls.Add(this.cmbStyle);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnIconBrowse);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtIcon);
            this.groupBox2.Controls.Add(this.chkIcon);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtMsg);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtTitle);
            this.groupBox2.Location = new System.Drawing.Point(12, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(707, 380);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customize your version";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(75, 31);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(335, 20);
            this.txtTitle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Title:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Message:";
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(75, 106);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(343, 65);
            this.txtMsg.TabIndex = 2;
            // 
            // chkIcon
            // 
            this.chkIcon.AutoSize = true;
            this.chkIcon.Checked = true;
            this.chkIcon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIcon.Location = new System.Drawing.Point(75, 83);
            this.chkIcon.Name = "chkIcon";
            this.chkIcon.Size = new System.Drawing.Size(75, 17);
            this.chkIcon.TabIndex = 4;
            this.chkIcon.Text = "Has Icon?";
            this.chkIcon.UseVisualStyleBackColor = true;
            this.chkIcon.CheckedChanged += new System.EventHandler(this.chkIcon_CheckedChanged);
            // 
            // txtIcon
            // 
            this.txtIcon.Location = new System.Drawing.Point(75, 57);
            this.txtIcon.Name = "txtIcon";
            this.txtIcon.Size = new System.Drawing.Size(254, 20);
            this.txtIcon.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Icon:";
            // 
            // btnIconBrowse
            // 
            this.btnIconBrowse.Location = new System.Drawing.Point(335, 55);
            this.btnIconBrowse.Name = "btnIconBrowse";
            this.btnIconBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnIconBrowse.TabIndex = 7;
            this.btnIconBrowse.Text = "Browse...";
            this.btnIconBrowse.UseVisualStyleBackColor = true;
            this.btnIconBrowse.Click += new System.EventHandler(this.btnIconBrowse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 586);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(662, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Thanks for downloading Clean Messages! The source code in this and the libraries " +
    "source code is OPEN SOURCE under the MIT License.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Style:";
            // 
            // cmbStyle
            // 
            this.cmbStyle.FormattingEnabled = true;
            this.cmbStyle.Items.AddRange(new object[] {
            "Default Style",
            "Info Style",
            "Warning Style",
            "Error Style",
            "Question Style",
            "Custom Style"});
            this.cmbStyle.Location = new System.Drawing.Point(75, 184);
            this.cmbStyle.Name = "cmbStyle";
            this.cmbStyle.Size = new System.Drawing.Size(214, 21);
            this.cmbStyle.TabIndex = 9;
            this.cmbStyle.SelectedIndexChanged += new System.EventHandler(this.cmbStyle_SelectedIndexChanged);
            // 
            // grpCustom
            // 
            this.grpCustom.Controls.Add(this.label17);
            this.grpCustom.Controls.Add(this.cmbTreated);
            this.grpCustom.Controls.Add(this.label16);
            this.grpCustom.Controls.Add(this.chkCustom);
            this.grpCustom.Controls.Add(this.btnCustomBrowse);
            this.grpCustom.Controls.Add(this.label9);
            this.grpCustom.Controls.Add(this.label8);
            this.grpCustom.Controls.Add(this.txtCustom);
            this.grpCustom.Controls.Add(this.label7);
            this.grpCustom.Controls.Add(this.txtB);
            this.grpCustom.Controls.Add(this.txtG);
            this.grpCustom.Controls.Add(this.label6);
            this.grpCustom.Controls.Add(this.txtR);
            this.grpCustom.Enabled = false;
            this.grpCustom.Location = new System.Drawing.Point(307, 184);
            this.grpCustom.Name = "grpCustom";
            this.grpCustom.Size = new System.Drawing.Size(394, 161);
            this.grpCustom.TabIndex = 10;
            this.grpCustom.TabStop = false;
            this.grpCustom.Text = "Custom Style";
            // 
            // txtR
            // 
            this.txtR.Location = new System.Drawing.Point(86, 29);
            this.txtR.Name = "txtR";
            this.txtR.Size = new System.Drawing.Size(42, 20);
            this.txtR.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "RGB Color:";
            // 
            // txtG
            // 
            this.txtG.Location = new System.Drawing.Point(147, 29);
            this.txtG.Name = "txtG";
            this.txtG.Size = new System.Drawing.Size(42, 20);
            this.txtG.TabIndex = 13;
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(211, 29);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(42, 20);
            this.txtB.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(134, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = ",";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(195, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = ",";
            // 
            // btnCustomBrowse
            // 
            this.btnCustomBrowse.Location = new System.Drawing.Point(313, 54);
            this.btnCustomBrowse.Name = "btnCustomBrowse";
            this.btnCustomBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnCustomBrowse.TabIndex = 13;
            this.btnCustomBrowse.Text = "Browse...";
            this.btnCustomBrowse.UseVisualStyleBackColor = true;
            this.btnCustomBrowse.Click += new System.EventHandler(this.button7_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Custom Image:";
            // 
            // txtCustom
            // 
            this.txtCustom.Location = new System.Drawing.Point(86, 56);
            this.txtCustom.Name = "txtCustom";
            this.txtCustom.Size = new System.Drawing.Size(221, 20);
            this.txtCustom.TabIndex = 11;
            // 
            // chkCustom
            // 
            this.chkCustom.AutoSize = true;
            this.chkCustom.Checked = true;
            this.chkCustom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCustom.Location = new System.Drawing.Point(86, 82);
            this.chkCustom.Name = "chkCustom";
            this.chkCustom.Size = new System.Drawing.Size(83, 17);
            this.chkCustom.TabIndex = 11;
            this.chkCustom.Text = "Has Image?";
            this.chkCustom.UseVisualStyleBackColor = true;
            this.chkCustom.CheckedChanged += new System.EventHandler(this.chkCustom_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 227);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Default Button:";
            // 
            // txtDefault
            // 
            this.txtDefault.Location = new System.Drawing.Point(98, 223);
            this.txtDefault.Name = "txtDefault";
            this.txtDefault.Size = new System.Drawing.Size(108, 20);
            this.txtDefault.TabIndex = 17;
            // 
            // chkAlt
            // 
            this.chkAlt.AutoSize = true;
            this.chkAlt.Checked = true;
            this.chkAlt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAlt.Location = new System.Drawing.Point(212, 254);
            this.chkAlt.Name = "chkAlt";
            this.chkAlt.Size = new System.Drawing.Size(66, 17);
            this.chkAlt.TabIndex = 18;
            this.chkAlt.Text = "Has Alt?";
            this.chkAlt.UseVisualStyleBackColor = true;
            this.chkAlt.CheckedChanged += new System.EventHandler(this.chkAlt_CheckedChanged);
            // 
            // txtAlt
            // 
            this.txtAlt.Location = new System.Drawing.Point(98, 249);
            this.txtAlt.Name = "txtAlt";
            this.txtAlt.Size = new System.Drawing.Size(108, 20);
            this.txtAlt.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 253);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Alt Button:";
            // 
            // txtOptional
            // 
            this.txtOptional.Enabled = false;
            this.txtOptional.Location = new System.Drawing.Point(98, 275);
            this.txtOptional.Name = "txtOptional";
            this.txtOptional.Size = new System.Drawing.Size(108, 20);
            this.txtOptional.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Optional Button:";
            // 
            // chkOptional
            // 
            this.chkOptional.AutoSize = true;
            this.chkOptional.Location = new System.Drawing.Point(212, 278);
            this.chkOptional.Name = "chkOptional";
            this.chkOptional.Size = new System.Drawing.Size(93, 17);
            this.chkOptional.TabIndex = 23;
            this.chkOptional.Text = "Has Optional?";
            this.chkOptional.UseVisualStyleBackColor = true;
            this.chkOptional.CheckedChanged += new System.EventHandler(this.checkOptional_CheckedChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(569, 351);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(132, 23);
            this.btnCreate.TabIndex = 5;
            this.btnCreate.Text = "Generate!";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(6, 351);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(132, 23);
            this.btnReset.TabIndex = 24;
            this.btnReset.Text = "Reset values";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // picIcon
            // 
            this.picIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picIcon.Location = new System.Drawing.Point(431, 47);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(24, 24);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picIcon.TabIndex = 25;
            this.picIcon.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(428, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Current Icon:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(502, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Current Image:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picImage
            // 
            this.picImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage.Location = new System.Drawing.Point(505, 46);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(32, 32);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picImage.TabIndex = 27;
            this.picImage.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::CleanMessagesExample.Properties.Resources.OnyxTypography;
            this.pictureBox3.Location = new System.Drawing.Point(191, 16);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(528, 184);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(191, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 25);
            this.label15.TabIndex = 29;
            this.label15.Text = "MADE FOR:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Treated as:";
            // 
            // cmbTreated
            // 
            this.cmbTreated.FormattingEnabled = true;
            this.cmbTreated.Items.AddRange(new object[] {
            "Default Style",
            "Info Style",
            "Warning Style",
            "Error Style",
            "Question Style"});
            this.cmbTreated.Location = new System.Drawing.Point(86, 105);
            this.cmbTreated.Name = "cmbTreated";
            this.cmbTreated.Size = new System.Drawing.Size(214, 21);
            this.cmbTreated.TabIndex = 29;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(83, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(271, 26);
            this.label17.TabIndex = 30;
            this.label17.Text = "This is used for checking what type of MsgBox it is!\r\nNo Styles are changed when " +
    "using one of these options";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 608);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clean Messages Example";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpCustom.ResumeLayout(false);
            this.grpCustom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnWarning;
        private System.Windows.Forms.Button btnError;
        private System.Windows.Forms.Button btnQuestion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.CheckBox chkOptional;
        private System.Windows.Forms.TextBox txtOptional;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAlt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkAlt;
        private System.Windows.Forms.TextBox txtDefault;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox grpCustom;
        private System.Windows.Forms.CheckBox chkCustom;
        private System.Windows.Forms.Button btnCustomBrowse;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCustom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.TextBox txtG;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtR;
        private System.Windows.Forms.ComboBox cmbStyle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnIconBrowse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIcon;
        private System.Windows.Forms.CheckBox chkIcon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbTreated;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
    }
}

