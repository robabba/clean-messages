﻿// CleanMessagesExample: Copyright (c) 2014 Robert Abba
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Clean_Messages;

namespace CleanMessagesExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            CleanMessage msg = new CleanMessage();
            msg.CleanMessageBoxDefault("::: Example Message :::", "This is an example message that you can set!", "Default", "Alternative", "Optional");
            msg.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CleanMessage msg = new CleanMessage();
            msg.CleanMessageBoxInfo("::: Example Message :::", "This is an example message that you can set!", "Default", "Alternative", "Optional");
            msg.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files (*.png, *.jpg)|*.png;*.jpg";
            open.Title = "Icon Image";
            open.Multiselect = false;

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCustom.Text = open.FileName;
                picImage.Image = new Bitmap(Image.FromFile(open.FileName));
            }
        }

        private void chkIcon_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIcon.Checked)
            {
                txtIcon.Enabled = true;
                btnIconBrowse.Enabled = true;
            }
            else {
                txtIcon.Enabled = false;
                btnIconBrowse.Enabled = false;
            }
        }

        private void chkAlt_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlt.Checked)
            {
                txtAlt.Enabled = true;
            }
            else {
                txtAlt.Enabled = false;
            }
        }

        private void checkOptional_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOptional.Checked)
            {
                txtOptional.Enabled = true;
            }
            else {
                txtOptional.Enabled = false;
            }
        }

        private void cmbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStyle.SelectedIndex == 5)
            {
                grpCustom.Enabled = true;
            }
            else {
                grpCustom.Enabled = false;
            }
        }

        private void chkCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCustom.Checked)
            {
                txtCustom.Enabled = true;
                btnCustomBrowse.Enabled = true;
            }
            else {
                txtCustom.Enabled = false;
                btnCustomBrowse.Enabled = false;
            }
        }

        private void btnIconBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files (*.png, *.jpg)|*.png;*.jpg";
            open.Title = "Icon Image";
            open.Multiselect = false;

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtIcon.Text = open.FileName;
                picIcon.Image = new Bitmap(Image.FromFile(open.FileName));
            }
        }

        private void btnWarning_Click(object sender, EventArgs e)
        {
            CleanMessage msg = new CleanMessage();
            msg.CleanMessageBoxWarning("::: Example Message :::", "This is an example message that you can set!", "Default", "Alternative", "Optional");
            msg.ShowDialog();
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            CleanMessage msg = new CleanMessage();
            msg.CleanMessageBoxError("::: Example Message :::", "This is an example message that you can set!", "Default", "Alternative", "Optional");
            msg.ShowDialog();
        }

        private void btnQuestion_Click(object sender, EventArgs e)
        {
            CleanMessage msg = new CleanMessage();
            msg.CleanMessageBoxQuestion("::: Example Message :::", "This is an example message that you can set!", "Default", "Alternative", "Optional");
            msg.ShowDialog();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                CleanMessage msg = new CleanMessage();
                string title = txtTitle.Text;
                string message = txtMsg.Text;
                Image icon = null;
                Image image = null;
                Color color = Color.White;

                if (chkIcon.Checked)
                {
                    icon = new Bitmap(Image.FromFile(txtIcon.Text));
                }

                string btnDefault = txtDefault.Text;
                string btnAlt = null;
                string btnOpt = null;

                if (chkAlt.Checked)
                {
                    btnAlt = txtAlt.Text;
                }

                if (chkOptional.Checked)
                {
                    btnOpt = txtOptional.Text;
                }

                if (grpCustom.Enabled)
                {
                    int r = Convert.ToInt32(txtR.Text);
                    int g = Convert.ToInt32(txtG.Text);
                    int b = Convert.ToInt32(txtB.Text);

                    color = Color.FromArgb(r, g, b);
                    if (chkCustom.Checked)
                    {
                        image = new Bitmap(Image.FromFile(txtCustom.Text));
                    }
                }

                switch (cmbStyle.SelectedIndex)
                {
                    case 0:
                        // Default
                        color = Color.FromArgb(100, 100, 100);
                        msg.CleanMessageBoxCustom(color, icon, title, null, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.NoStyle);
                        break;
                    case 1:
                        // Info
                        color = Color.LightSeaGreen;
                        msg.CleanMessageBoxCustom(color, icon, title, null, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Info);
                        break;
                    case 2:
                        // Warning
                        color = Color.Goldenrod;
                        msg.CleanMessageBoxCustom(color, icon, title, null, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Warning);
                        break;
                    case 3:
                        // Error
                        color = Color.Crimson;
                        msg.CleanMessageBoxCustom(color, icon, title, null, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Error);
                        break;
                    case 4:
                        // Question
                        color = Color.OliveDrab;
                        msg.CleanMessageBoxCustom(color, icon, title, null, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Question);
                        break;
                    case 5:
                        // Custom
                        switch (cmbTreated.SelectedIndex)
                        {
                            case 0:
                                // Treated as: Default
                                msg.CleanMessageBoxCustom(color, icon, title, image, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.NoStyle);
                                break;
                            case 1:
                                // Treated as: Info
                                msg.CleanMessageBoxCustom(color, icon, title, image, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Info);
                                break;
                            case 2:
                                // Treated as: Warning
                                msg.CleanMessageBoxCustom(color, icon, title, image, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Warning);
                                break;
                            case 3:
                                // Treated as: Error
                                msg.CleanMessageBoxCustom(color, icon, title, image, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Error);
                                break;
                            case 4:
                                // Treated as: Question
                                msg.CleanMessageBoxCustom(color, icon, title, image, message, btnDefault, btnAlt, btnOpt, CleanMessageStyle.Question);
                                break;
                        }

                        break;
                }

                msg.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtTitle.Text = "";
            chkIcon.Checked = true;
            txtIcon.Enabled = true;
            txtIcon.Text = "";
            btnIconBrowse.Enabled = true;
            txtMsg.Text = "";
            cmbStyle.SelectedIndex = 0;
            cmbStyle.Text = "";
            grpCustom.Enabled = false;
            txtR.Text = "";
            txtG.Text = "";
            txtB.Text = "";
            txtCustom.Text = "";
            chkCustom.Checked = true;
            cmbTreated.SelectedIndex = 0;
            cmbTreated.Text = "";
            txtDefault.Text = "";
            txtAlt.Text = "";
            txtAlt.Enabled = true;
            chkAlt.Checked = true;
            txtOptional.Text = "";
            txtOptional.Enabled = false;
            chkOptional.Checked = false;
            picIcon.Image = null;
            picImage.Image = null;
        }   
    }
}
