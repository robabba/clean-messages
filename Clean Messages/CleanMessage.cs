﻿// Clean Messages: Copyright (c) 2014 Robert Abba
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Clean_Messages
{
    public enum CleanMessageStyle { 
        NoStyle,
        Info,
        Warning,
        Error,
        Question
    };

    public class CleanMessage : Form
    {
        #region ivars and objects
        // ------------------
        // Init private ivars
        // ------------------
        private bool warning = false;
        private bool error = false;
        private bool question = false;
        private bool info = false;
        private bool _default = false;
        private bool canDragAround = false;
        private Point dragPoint = new Point();

        // --------------------
        // Init private objects
        // --------------------
        private Panel topDraggable = new Panel();
        private Panel centerInfo = new Panel();
        private Panel bottomControllers = new Panel();
        private Label topTitle = new Label();
        private Label centerText = new Label();
        private PictureBox style = new PictureBox();
        private Button buttonClose = new Button();

        // -------------------
        // Init public objects
        // -------------------
        public Button buttonDefault = new Button();
        public Button buttonAlt = new Button();
        public Button buttonThird = new Button();
        #endregion

        #region Constructor
        public CleanMessage()
        {
            // ---------------------------------
            // Set default properties of objects
            // ---------------------------------

            // Form Default Properties
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Size = new Size(375, 165);
            this.BackColor = Color.FromArgb(100, 100, 100);
            this.Padding = new Padding(2);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ForeColor = Color.White;
            this.Font = new Font("Segoe UI", 9);

            if (!(centerText.Size.Width < 280))
            {
                this.Size = new Size(centerText.Size.Width + 20, this.Height);
            }

            if (!(centerText.Size.Height < 53))
            {
                this.Size = new Size(this.Width, this.Size.Height + 20);
            }

            // topDraggable Default Properties
            topDraggable.Dock = DockStyle.Top;
            topDraggable.Size = new Size(this.Width - 4, 30);
            topDraggable.Height = 25;
            topDraggable.BackColor = Color.FromArgb(40, 40, 40);

            // bottomControllers Default Properties
            bottomControllers.Dock = DockStyle.Bottom;
            bottomControllers.Size = new Size(370, 38);
            bottomControllers.Height = 40;
            bottomControllers.BackColor = Color.FromArgb(40, 40, 40);

            // centerInfo Default Properties
            centerInfo.Dock = DockStyle.Fill;
            centerInfo.Size = new Size(370, 134);
            centerInfo.BackColor = Color.FromArgb(50, 50, 50);

            // topTitle Default Properties
            topTitle.Anchor = AnchorStyles.Top & AnchorStyles.Left;
            topTitle.ForeColor = Color.White;
            topTitle.AutoSize = true;
            topTitle.TextAlign = ContentAlignment.TopLeft;
            topTitle.Location = new Point(3, 3);
            topTitle.Text = "::: Example Message Box :::".ToUpper();

            // centerText Default Properties
            centerText.Anchor = AnchorStyles.Right & AnchorStyles.Left;
            centerText.ForeColor = Color.White;
            centerText.Location = new Point(75, 35);
            centerText.Text = "Some message that you would want here!";
            centerText.Size = new Size(278, 53);
            centerInfo.AutoSize = true;

            // buttonDefault Default Properties
            buttonDefault.Anchor = AnchorStyles.Right;
            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.OK & System.Windows.Forms.DialogResult.Yes;
            buttonDefault.AutoSize = true;
            buttonDefault.FlatStyle = FlatStyle.Flat;
            buttonDefault.FlatAppearance.BorderColor = Color.FromArgb(100, 100, 100);
            buttonDefault.FlatAppearance.BorderSize = 1;
            buttonDefault.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
            buttonDefault.FlatAppearance.MouseOverBackColor = Color.FromArgb(60, 60, 60);
            buttonDefault.Location = new Point(206, 8);
            buttonDefault.Size = new Size(75, 23);
            buttonDefault.Text = "Default";

            // buttonAlt Default Properties
            buttonAlt.Anchor = AnchorStyles.Right;
            buttonAlt.AutoSize = true;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No & System.Windows.Forms.DialogResult.Cancel;
            buttonAlt.FlatStyle = FlatStyle.Flat;
            buttonAlt.FlatAppearance.BorderColor = Color.FromArgb(60, 60, 60);
            buttonAlt.FlatAppearance.BorderSize = 1;
            buttonAlt.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
            buttonAlt.FlatAppearance.MouseOverBackColor = Color.FromArgb(60, 60, 60);
            buttonAlt.Location = new Point(287, 8);
            buttonAlt.Size = new Size(75, 23);
            buttonAlt.Text = "Alt";

            // buttonThird Default Properties
            buttonThird.Anchor = AnchorStyles.Right;
            buttonThird.AutoSize = true;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Ignore & System.Windows.Forms.DialogResult.Abort;
            buttonThird.FlatStyle = FlatStyle.Flat;
            buttonThird.FlatAppearance.BorderColor = Color.FromArgb(60, 60, 60);
            buttonThird.FlatAppearance.BorderSize = 1;
            buttonThird.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
            buttonThird.FlatAppearance.MouseOverBackColor = Color.FromArgb(60, 60, 60);
            buttonThird.Location = new Point(125, 8);
            buttonThird.Size = new Size(75, 23);
            buttonThird.Visible = false;
            buttonThird.Text = "Third";

            // buttonClose Properties
            buttonClose.Anchor = AnchorStyles.Top & AnchorStyles.Right;
            buttonClose.FlatStyle = FlatStyle.Flat;
            buttonClose.FlatAppearance.BorderSize = 0;
            buttonClose.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
            buttonClose.FlatAppearance.MouseOverBackColor = Color.FromArgb(60, 60, 60);
            buttonClose.Image = new Bitmap(Clean_Messages.Properties.Resources.closeWin);
            buttonClose.ImageAlign = ContentAlignment.MiddleCenter;
            buttonClose.Location = new Point(344, 2);
            buttonClose.Size = new Size(25, 22);
            buttonClose.Text = "";

            // style Properties
            style.Anchor = AnchorStyles.Left;
            style.Location = new Point(37, 35);
            style.Size = new Size(32, 32);
            style.SizeMode = PictureBoxSizeMode.Zoom;

            // ----------------------
            // Add Components to Form
            // ----------------------   
            // Panel Controls
            topDraggable.Controls.Add(topTitle);
            topDraggable.Controls.Add(buttonClose);

            centerInfo.Controls.Add(style);
            centerInfo.Controls.Add(centerText);

            bottomControllers.Controls.Add(buttonAlt);
            bottomControllers.Controls.Add(buttonDefault);
            bottomControllers.Controls.Add(buttonThird);

            // Form controls
            this.Controls.Add(topDraggable);
            this.Controls.Add(bottomControllers);
            this.Controls.Add(centerInfo);

            // ----------------------------------------------------------------
            // Set Handlers for interactable objects that the user can't access
            // ----------------------------------------------------------------
            buttonClose.Click += buttonClose_Click;
            topDraggable.MouseDown += topDraggable_MouseDown;
            topDraggable.MouseMove += topDraggable_MouseMove;
            topDraggable.MouseUp += topDraggable_MouseUp;

            // Load
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CleanMessage
            // 
            this.Name = "CleanMessage";
            this.ResumeLayout(true);
        }
        #endregion

        #region is[Style] getters

        public bool isWarning
        {
            get
            {
                return warning;
            }
            private set
            {
                warning = value;
            }
        }

        public bool isError
        {
            get
            {
                return error;
            }
            private set
            {
                error = value;
            }
        }

        public bool isQuestion
        {
            get
            {
                return question;
            }
            private set
            {
                question = value;
            }
        }

        public bool isInfo
        {
            get
            {
                return info;
            }
            private set
            {
                info = value;
            }
        }

        public bool isDefault
        {
            get
            {
                return _default;
            }
            private set
            {
                _default = value;
            }
        }

        #endregion

        #region CleanMessageBoxes
        public void CleanMessageBoxCustom(Color outline, string title, Image styleImg, string message, string btnDefault, string btnAlt, string btnOther, CleanMessageStyle inheritTraits)
        {
            this.BackColor = outline;
            buttonDefault.FlatAppearance.BorderColor = outline;

            topTitle.Text = title.ToUpper();

            if (styleImg != null)
            {
                style.Image = new Bitmap(styleImg);
            }

            centerText.Text = message;
            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            switch (inheritTraits)
            {
                case CleanMessageStyle.NoStyle:
                    _default = true;
                    error = false;
                    question = false;
                    info = false;
                    warning = false;
                    buttonDefault.FlatAppearance.BorderColor = outline;
                    break;
                case CleanMessageStyle.Error:
                    _default = false;
                    error = true;
                    question = false;
                    info = false;
                    warning = false;
                    buttonDefault.FlatAppearance.BorderColor = outline;
                    break;
                case CleanMessageStyle.Question:
                    _default = false;
                    error = false;
                    question = true;
                    info = false;
                    warning = false;
                    buttonDefault.FlatAppearance.BorderColor = outline;
                    break;
                case CleanMessageStyle.Info:
                    _default = false;
                    error = false;
                    question = false;
                    info = true;
                    warning = false;
                    buttonDefault.FlatAppearance.BorderColor = outline;
                    break;
                case CleanMessageStyle.Warning:
                    _default = false;
                    error = false;
                    question = false;
                    info = false;
                    warning = true;
                    buttonDefault.FlatAppearance.BorderColor = outline;
                    break;
                default:
                    _default = true;
                    error = false;
                    question = false;
                    info = false;
                    warning = false;
                    break;
            }

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;

        }

        public void CleanMessageBoxError(string title, string message, string btnDefault, string btnAlt, string btnOther)
        {
            this.BackColor = Color.Crimson;
            centerText.Text = message;
            style.Image = Clean_Messages.Properties.Resources.error;

            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            _default = false;
            error = true;
            question = false;
            info = false;
            warning = false;
            buttonDefault.FlatAppearance.BorderColor = Color.Crimson;

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;
        }

        public void CleanMessageBoxInfo(string title, string message, string btnDefault, string btnAlt, string btnOther)
        {
            this.BackColor = Color.LightSeaGreen;
            centerText.Text = message;
            style.Image = Clean_Messages.Properties.Resources.info;

            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            _default = false;
            error = false;
            question = false;
            info = true;
            warning = false;
            buttonDefault.FlatAppearance.BorderColor = Color.LightSeaGreen;

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;
        }

        public void CleanMessageBoxQuestion(string title, string message, string btnDefault, string btnAlt, string btnOther)
        {
            this.BackColor = Color.OliveDrab;
            centerText.Text = message;
            style.Image = Clean_Messages.Properties.Resources.Question;

            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            _default = false;
            error = false;
            question = true;
            info = false;
            warning = false;
            buttonDefault.FlatAppearance.BorderColor = Color.OliveDrab;

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;
        }

        public void CleanMessageBoxWarning(string title, string message, string btnDefault, string btnAlt, string btnOther)
        {
            this.BackColor = Color.Goldenrod;
            centerText.Text = message;
            style.Image = Clean_Messages.Properties.Resources.warning;

            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            _default = false;
            error = false;
            question = false;
            info = false;
            warning = true;
            buttonDefault.FlatAppearance.BorderColor = Color.Goldenrod;

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;
        }

        public void CleanMessageBoxDefault(string title, string message, string btnDefault, string btnAlt, string btnOther)
        {
            this.BackColor = Color.FromArgb(100,100,100);
            centerText.Text = message;
            style.Image = null;

            buttonDefault.Text = btnDefault;

            if (btnAlt != null)
            {
                buttonAlt.Text = btnAlt;
            }
            else
            {
                buttonAlt.Visible = false;
                buttonDefault.Location = new Point(287, 8);
            }

            if (btnOther != null)
            {
                buttonThird.Visible = true;
                buttonThird.Text = btnOther;
            }

            _default = true;
            error = false;
            question = false;
            info = false;
            warning = false;
            buttonDefault.FlatAppearance.BorderColor = Color.FromArgb(100, 100, 100);

            buttonDefault.DialogResult = System.Windows.Forms.DialogResult.Yes;
            buttonAlt.DialogResult = System.Windows.Forms.DialogResult.No;
            buttonThird.DialogResult = System.Windows.Forms.DialogResult.Retry;
        }
        #endregion

        #region Handlers for privately owned objects

        void topDraggable_MouseMove(object sender, MouseEventArgs e)
        {
            if (canDragAround)
            {
                this.Location = new Point(this.Location.X + e.X - dragPoint.X, this.Location.Y + e.Y - dragPoint.Y);
            }
        }

        void topDraggable_MouseDown(object sender, MouseEventArgs e)
        {
            canDragAround = true;
            dragPoint = new Point(e.X, e.Y);
        }

        void topDraggable_MouseUp(object sender, MouseEventArgs e)
        {
            canDragAround = false;
        }

        void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
        
}
